//
//  ConsoleManager.swift
//  MyGames
//
//  Created by Paulo Gutemberg on 24/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import Foundation
import CoreData

class ConsoleManager {
	
	static let shared = ConsoleManager()
	var consoles: [Console] = []
	
	private init() {
		
	}
	
	func loadConsoles(com contexo: NSManagedObjectContext){
		let fetchRequest: NSFetchRequest<Console> = Console.fetchRequest()
		let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
		fetchRequest.sortDescriptors = [sortDescriptor]
		do {
			consoles = try contexo.fetch(fetchRequest)
		} catch let error{
			print(error)
		}
	}
	
	func deleteConsole(indice: Int, contexto: NSManagedObjectContext){
		let console = consoles[indice]
		contexto.delete(console)
		do {
			try contexto.save()
			consoles.remove(at: indice)
		} catch let erro {
			print(erro)
		}
	}
	
}
