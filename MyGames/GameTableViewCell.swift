//
//  GameTableViewCell.swift
//  MyGames
//
//  Created by Paulo Gutemberg on 15/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class GameTableViewCell: UITableViewCell {

	@IBOutlet weak var ivCover: UIImageView!
	@IBOutlet weak var lbName: UILabel!
	@IBOutlet weak var lbConsole: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func preparaCelula(com joguinho: Game){
		self.lbName.text = joguinho.title ?? "No Name"
		self.lbConsole.text = joguinho.console?.name ?? "No Name"
		if let img = joguinho.cover as? UIImage {
			self.ivCover.image = img
		}else{
			self.ivCover.image = UIImage(named: "noCover")
		}
	}

}
