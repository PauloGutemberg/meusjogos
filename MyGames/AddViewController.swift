//
//  AddViewController.swift
//  MyGames
//
//  Created by Paulo Gutemberg on 15/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {

	@IBOutlet weak var tfTitle: UITextField!
	@IBOutlet weak var tfConsole: UITextField!
	
	@IBOutlet weak var dpReleaseDate: UIDatePicker!
	@IBOutlet weak var ivCover: UIImageView!
	
	@IBOutlet weak var btAdd: UIButton!
	@IBOutlet weak var btCover: UIButton!
	
	var game: Game!
	
	lazy var pickerView: UIPickerView = {
		let pickerView = UIPickerView()
		pickerView.delegate = self
		pickerView.dataSource = self
		pickerView.backgroundColor = .white
		return pickerView
	}()
	
	var consolesManager = ConsoleManager.shared
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		if game != nil {
			title = "Editar jogo"
			btAdd.setTitle("Alterar", for: .normal)
			
			tfTitle.text = game.title
			if let console = game.console, let index = consolesManager.consoles.firstIndex(of: console) {
				tfConsole.text = console.name
				pickerView.selectRow(index, inComponent: 0, animated: true)
			}
			ivCover.image = game.cover as? UIImage
			if let releaseDate = game.releaseDate {
				dpReleaseDate.date = releaseDate
			}
			if game.cover != nil {
				btCover.setTitle(nil, for: .normal)
			}
			
		}
		prepareConsole()
    }
	
	
	func prepareConsole(){
		let barraDeFerramenta = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
		barraDeFerramenta.tintColor = UIColor(named: "main")
		
		let btnCancelar = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelar))
		let btnPronto = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(pronto))
		let btnEspacoFlexivel = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		
		barraDeFerramenta.items = [btnCancelar , btnEspacoFlexivel ,btnPronto]
		
		tfConsole.inputView = pickerView
		tfConsole.inputAccessoryView = barraDeFerramenta

	}
	
	@objc func cancelar(){
		tfConsole.resignFirstResponder()
	}
	
	@objc func pronto(){
		
		tfConsole.text = consolesManager.consoles[pickerView.selectedRow(inComponent: 0)].name
		
		cancelar()
	}
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		consolesManager.loadConsoles(com: contexto)
		
	}
	
	@IBAction func addCover(_ sender: UIButton) {
		
		let alerta = UIAlertController(title: "Selecione o poster", message: "De onde você quer escolher o poster", preferredStyle: .actionSheet)
		if UIImagePickerController.isSourceTypeAvailable(.camera){
			let acaoCamera = UIAlertAction(title: "Câmera", style: .default) { (acao: UIAlertAction) in
				self.selecionarFoto(sourceType: .camera)
			}
			alerta.addAction(acaoCamera)
		}
		
		let acaoBiblioteca = UIAlertAction(title: "Biblioteca de fotos", style: .default) { (acao: UIAlertAction) in
			self.selecionarFoto(sourceType: .photoLibrary)
		}
		alerta.addAction(acaoBiblioteca)
		
		let acaoAlbum = UIAlertAction(title: "Album de fotos", style: .default) { (acao: UIAlertAction) in
			self.selecionarFoto(sourceType: .savedPhotosAlbum)
		}
		alerta.addAction(acaoAlbum)
		let acaoCancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
		alerta.addAction(acaoCancelar)
		present(alerta, animated: true, completion: nil)
		
	}
	
	func selecionarFoto(sourceType: UIImagePickerController.SourceType){
		
		let imagePiker = UIImagePickerController()
		imagePiker.sourceType = sourceType
		imagePiker.delegate = self
		imagePiker.navigationBar.tintColor = UIColor(named: "main")
		present(imagePiker, animated:true, completion: nil)
	}
	
	@IBAction func addGame(_ sender: Any) {
		if game == nil {
			game = Game(context: contexto)
		}
		game.title = self.tfTitle.text
		game.releaseDate = self.dpReleaseDate.date
		
		if !self.tfConsole.text!.isEmpty {
			let console = consolesManager.consoles[pickerView.selectedRow(inComponent: 0)]
			game.console = console
		}
		game.cover = ivCover.image
		do{
			try contexto.save()
		}catch{
			print(error.localizedDescription)
		}
		navigationController?.popViewController(animated: true)
	}
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddViewController: UIPickerViewDelegate {
	
}

extension AddViewController: UIPickerViewDataSource {
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return consolesManager.consoles.count
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		let console = consolesManager.consoles[row]
		return console.name
	}
	
}

extension AddViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		let img = info[.originalImage] as? UIImage
		ivCover.image = img
		btCover.setTitle(nil, for: .normal)
		dismiss(animated: true, completion: nil)
	}
}
