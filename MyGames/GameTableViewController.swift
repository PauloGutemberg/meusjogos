//
//  GameTableViewController.swift
//  MyGames
//
//  Created by Paulo Gutemberg on 15/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit
import CoreData

class GameTableViewController: UITableViewController {

	var fetchedResultController: NSFetchedResultsController<Game>!
	let controleDePesquisa = UISearchController(searchResultsController: nil)
	var label = UILabel()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		//self.navigationController?.navigationBar.tintColor = UIColor(named: "main")
		//self.navigationController?.navigationBar.backgroundColor = UIColor(named: "main")
		self.label.text = "Você não tem jogos cadastrados"
		self.label.textAlignment = .center
		self.configControleDePesquisa()
		self.loadGames()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		tableView.reloadData()
	}
	
	func configControleDePesquisa(){
		controleDePesquisa.searchResultsUpdater = self
		controleDePesquisa.dimsBackgroundDuringPresentation = false
		controleDePesquisa.searchBar.tintColor = .white
		controleDePesquisa.searchBar.barTintColor = .white
		
		navigationItem.searchController = controleDePesquisa
		controleDePesquisa.searchBar.delegate = self
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "gameSegue" {
			let vc = segue.destination as! GameViewController
			if let games = fetchedResultController.fetchedObjects {
				vc.game = games[tableView.indexPathForSelectedRow!.row]
			}
		}
	}
	
	func loadGames(filtro: String = ""){
		//FetchRequest e lib do Core data, importamte nao esquecer de importar essa Lib
		let fetchRequest: NSFetchRequest<Game> = Game.fetchRequest()
		let sortDescritor = NSSortDescriptor(key: "title", ascending: true)
		fetchRequest.sortDescriptors = [sortDescritor]
		
		if !filtro.isEmpty {
			let predicate = NSPredicate(format: "title contains [c] %@", filtro)
			fetchRequest.predicate = predicate
		}
		
		self.fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: contexto, sectionNameKeyPath: nil, cacheName: nil)
		self.fetchedResultController.delegate = self
		do{
			try fetchedResultController.performFetch()
		}catch{
			print(error.localizedDescription)
		}
	}
	
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //lista de objetos recuperados
		//fetchedResultController.fetchedObjects
		let contador = self.fetchedResultController.fetchedObjects?.count ?? 0
		if contador == 0 {
			self.tableView.backgroundView = self.label
		}else{
			self.tableView.backgroundView = nil
		}
		
        return contador
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GameTableViewCell
		
		guard let game = self.fetchedResultController.fetchedObjects?[indexPath.row] else { return cell }
		cell.preparaCelula(com: game)
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
			guard let game = fetchedResultController.fetchedObjects?[indexPath.row] else {
				return
			}
			contexto.delete(game)
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GameTableViewController: NSFetchedResultsControllerDelegate {
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		//quando fizer alguma edicao de game esse metodo vai ser disparado, mesmo que nao esteja nessa classe
		
		switch type {
		case .delete:
			if let indeXPath = indexPath {
				tableView.deleteRows(at: [indeXPath], with: .fade)
			}
			break
		default:
			self.tableView.reloadData()
		}
	}
}

extension GameTableViewController: UISearchBarDelegate {
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		loadGames()
		tableView.reloadData()
	}
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		loadGames(filtro: searchBar.text!)
		tableView.reloadData()
	}
	
}

extension GameTableViewController: UISearchResultsUpdating {
	
	func updateSearchResults(for searchController: UISearchController) {
		
	}
	
	
}
