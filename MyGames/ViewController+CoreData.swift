//
//  ViewController+CoreData.swift
//  MyGames
//
//  Created by Paulo Gutemberg on 22/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit
import CoreData

extension UIViewController {
	//Classe que represanta um context no core data é NSManagedObjectContext
	var contexto: NSManagedObjectContext {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		return appDelegate.persistentContainer.viewContext
	}
	
	
}
