//
//  ConsoleTableViewController.swift
//  MyGames
//
//  Created by Paulo Gutemberg on 15/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class ConsoleTableViewController: UITableViewController {

	var consoleManager = ConsoleManager.shared
	let controleDePesquisa = UISearchController(searchResultsController: nil)
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.loadConsoles()
		
	}
	
	func loadConsoles(){
		consoleManager.loadConsoles(com: contexto)
		tableView.reloadData()
	}
	
	func mostrarAlerta(com console: Console?){
		let titulo = console == nil ? "Adicionar" : "Editar"
		let alerta = UIAlertController(title: titulo + " plataforma", message: nil, preferredStyle: .alert)
		alerta.addTextField { (campoTexto) in
			campoTexto.placeholder = "Nome da plataforma"
			if let nome = console?.name {
				campoTexto.text = nome
			}
		}
		
		alerta.addAction(UIAlertAction(title: titulo, style: .default, handler: { (acao) in
			let console = console ?? Console(context: self.contexto)
			console.name = alerta.textFields?.first?.text
			do{
				try self.contexto.save()
				self.loadConsoles()
			}catch let erro{
				print(erro)
			}
		}))
		
		alerta.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
		alerta.view.tintColor = UIColor(named: "second")
		present(alerta, animated: true, completion: nil)
	}
	
	@IBAction func addConsole(_ sender: Any) {
		mostrarAlerta(com: nil)
	}
	
    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
		return consoleManager.consoles.count
    }
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let console = consoleManager.consoles[indexPath.row]
		mostrarAlerta(com: console)
		tableView.deselectRow(at: indexPath, animated: true)
	}
    
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			consoleManager.deleteConsole(indice: indexPath.row, contexto: contexto)
			tableView.deleteRows(at: [indexPath], with: .fade)
		}
	}
	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		
		let console =  consoleManager.consoles[indexPath.row]
		cell.textLabel?.text = console.name

        return cell
    }

}
