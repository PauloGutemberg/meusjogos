//
//  GameViewController.swift
//  MyGames
//
//  Created by Paulo Gutemberg on 15/03/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
	@IBOutlet weak var lbTitle: UILabel!
	@IBOutlet weak var lbConsole: UILabel!
	@IBOutlet weak var lbReleaseDate: UILabel!
	
	@IBOutlet weak var ivCover: UIImageView!
	
	var game: Game!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
    }
    
	func loadGame(com game: Game) {
		lbTitle.text = game.title
		lbConsole.text = game.console?.name
		if let releaseDate = game.releaseDate {
			let formatter = DateFormatter()
			formatter.dateStyle = .long
			formatter.locale = Locale(identifier: "pt-BR")
			lbReleaseDate.text = "Lançamento: " + formatter.string(from: releaseDate)
		}
		if let img = game.cover as? UIImage {
			ivCover.image = img
		}else{
			ivCover.image = UIImage(named: "noCoverFull")
		}
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let vc = segue.destination as! AddViewController
		vc.game = game
	}
	
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		loadGame(com: game)
	}
	
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
